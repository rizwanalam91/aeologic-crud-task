const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");
const bodyParser = require("body-parser");
const app = express();

//Bring all routes
const person = require("./routes/personRoute");

//Middleware for body-parser
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

//mongoDB configuration
const db = require("./config/personURL").mongoURL;

//Attempt to connect to database
mongoose
  .connect(db, { useNewUrlParser: true , useUnifiedTopology:true, useFindAndModify: false})
  .then(() => console.log("MongoDB connected successfully"))
  .catch(err => console.log(err));

  //Passport middleware
app.use(passport.initialize());

//Config for JWT strategy
require("./strategies/jsonwtStrategy")(passport);


//just for testing  -> route
app.get("/", (req, res) => {
  res.json({test : "Hey Rizwan! default route is tested successfully"})
});

//actual routes
app.use("/api/person", person);

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server is running at ${port}...`)
});
