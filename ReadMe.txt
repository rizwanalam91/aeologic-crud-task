
1.Route of register user.
http://localhost:3000/api/person/createUser

2.Route of login user.
http://localhost:3000/api/person/login

3.Route of getting single user
http://localhost:3000/api/person/getUser/:id

4.Route of getting all users
http://localhost:3000/api/person/getAllUsers

5.Route to update details of user
http://localhost:3000/api/person/updateUser/:id

4.Route to delete user
http://localhost:3000/api/person/deleteUser/:id

