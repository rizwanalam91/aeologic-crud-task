const express = require("express");
const router = express.Router();
const personController = require("../controllers/personController")
const passport = require("passport");
const isSignedIn = passport.authenticate("jwt", { session: false })

// just for testing route.
router.get("/", personController.testRoute )

// route to create the users.
router.post("/createUser", personController.createUser);

// route to login the users.
router.post("/login",personController.signIn);

// route to fetch one user.
router.get("/getUser/:id",isSignedIn,personController.getUser )

// route to fetch all the users.
router.get('/getAllUsers',isSignedIn,personController.getAllUsers);

// route to update the details of any user.
router.put("/updateUser/:id",isSignedIn,personController.updateUser)

// route to get all the users.
router.delete("/deleteUser/:id",isSignedIn,personController.deleteUser)

module.exports = router;
