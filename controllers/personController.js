//Import Schema for Person to Register
const Person = require("../model/PersonModel");
const bcrypt = require("bcryptjs");
const jsonwt = require("jsonwebtoken");
const key = require("../config/personURL");


// just for testing
exports.testRoute = (req,res) => {
    res.json({test : "Hey Rizwan! Auth file is tested successfully"})
}


// create and save a new user.
exports.createUser =  (req, res) => {
    Person.findOne({ email: req.body.email })
      .then(person => {
        if (person) {
          return res
            .status(400)
            .json({ emailError: "Email is already registered in our system" });
        } else {
          const newPerson = new Person({
            name: req.body.name,
            email: req.body.email,
            password:req.body.password,
            address: req.body.address,
            company: req.body.company,
            mobile: req.body.mobile
          });
         //Encrypt password using bcrypt
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newPerson.password, salt, (err, hash) => {
              if(err) throw err;
            newPerson.password = hash;
            newPerson.save()
            .then(data => {
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Something went wrong while creating new user."
                });
            });
            })
          })
         
        }
      })
      .catch(err => console.log(err));
  }

//sign in 
  exports.signIn = (req,res) => {
    const email = req.body.email;
    const password = req.body.password;

    Person.findOne({email})
    .then(person => {
      if(!person){
        return res.status(404).json({emailError: "User not found with this email"})
      }
      bcrypt.compare(password, person.password)
      .then(isCorrect => {
        if(isCorrect) {
          // res.json({ success: "User is able to login successfully" });
            //use payload and create token for user
            const payload = {
              id: person.id,
              name: person.name,
              email: person.email
            };
            jsonwt.sign(
              payload,
              key.secret,
              { expiresIn: 3600 },
              (err, token) => {
                res.json({
                  success: true,
                  token: "Bearer " + token
                });
              }
            );
        } else {
          res.status(400).json({ passwordError: "Password is not correct" });
        }
      })
      .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
  }




// fetch a single user from the database
exports.getUser = (req,res) => {
    Person.findById(req.params.id)
    .then(user => {
      if(!user) {
          return res.status(404).send({
              message: "User not found with id " + req.params.id
          });            
      }
      res.send(user);
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          return res.status(404).send({
              message: "User not found with id " + req.params.id
          });                
      }
      return res.status(500).send({
          message: "Error getting user with id " + req.params.id
      });
  });
  }


//fetch all the users from the database
exports.getAllUsers =  (req, res) => {
    Person.find({}, (err, users) => {
      var userMap = {};
  
      users.forEach((user) => {
        userMap[user._id] = user;
      });
  
      res.send(userMap);  
    });
  }


//update the details of a particular user
exports.updateUser =  (req,res) => {
    var conditions = {_id: req.params.id};

    Person.update(conditions, req.body)
    .then(doc => {
        if(!doc) { return res.status(404).end()}
        return res.status(200).json(doc)
    })
    .catch(err => next(err))
}


//delete a particular user
exports.deleteUser =  (req,res) => {
    Person.findByIdAndRemove(req.params.id)
    .exec()
    .then(doc => {
        if(!doc) {return res.status(404).end()}
        return res.status(200).json({Message : 'successfully deleted.'}).end()
    })
    .catch(err => console.log(err))
}